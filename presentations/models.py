from django.db import models
from django.urls import reverse


class Status(models.Model):
    """
    The Status model provides a status to a Presentation, which
    can be SUBMITTED, APPROVED, or REJECTED.

    Status is a Value Object and, therefore, does not have a
    direct URL to view it.
    """

    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ("id",)  # Default ordering for Status
        verbose_name_plural = "statuses"  # Fix the pluralization


class Presentation(models.Model):
    """
    The Presentation model represents a presentation that a person
    wants to give at the conference.
    """

    presenter_name = models.CharField(max_length=150)
    company_name = models.CharField(max_length=150, null=True, blank=True)
    presenter_email = models.EmailField()

    title = models.CharField(max_length=200)
    synopsis = models.TextField()
    created = models.DateTimeField(auto_now_add=True)

    status = models.ForeignKey(
        Status,
        related_name="presentations",
        on_delete=models.PROTECT,
    )

    conference = models.ForeignKey(
        "events.Conference",
        related_name="presentations",
        on_delete=models.CASCADE,
    )

    # defining a method called approve on the Presentation model
    # self parameter refers to the Presentation model that is being called on
    def approve(self):
        # Using ORM to fetch the status object if the name is APPROVED
        # We are also assigning this object to the variable approved_status
        approved_status = Status.objects.get(name="APPROVED")
        # assigning approved_status object to the status atrribute of the Presentation model instance
        self.status = approved_status
        # saves the changes
        self.save()

    def reject(self):
        # fetching the 'status' object when the name is rejected and assigning it a variable - rejected_status
        rejected_status = Status.objects.get(name="REJECTED")
        # assigning the rejected_status object to the status atrribute of the Presentation model instance
        # changes the status to REJECTED
        self.status = rejected_status
        # Saves our changes
        self.save()

    def get_api_url(self):
        return reverse("api_show_presentation", kwargs={"id": self.id})

    def __str__(self):
        return self.title

    class Meta:
        ordering = ("title",)  # Default ordering for presentation
