import requests

from events.keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photos(city, state):
    url = "https://api.pexels.com/v1/"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f"{city} {state}",
        "per_page": 15,  # adjust as necessary
    }
    response = requests.get(url, headers=headers, params=params)

    if response.status_code == 200:
        return response.json()  # returns the photos as JSON
    else:
        return None  # or handle the error


def get_weather(city):
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)

    if response.status_code == 200:
        return response.json()  # returns the weather data as JSON
    else:
        return None  # or handle the error
