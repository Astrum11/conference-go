from django.http import JsonResponse
from attendees.models import Attendee


def api_list_attendees(request, conference_id):
    response = []
    attendees = Attendee.objects.filter(conference_id=conference_id)
    for attendee in attendees:
        response.append(
            {
                "name": attendee.name,
                "href": attendee.get_api_url(),
            }
        )
    """
    Lists the attendees names and the link to the attendee
    for the specified conference id.

    Returns a dictionary with a single key "attendees" which
    is a list of attendee names and URLS. Each entry in the list
    is a dictionary that contains the name of the attendee and
    the link to the attendee's information.

    {
        "attendees": [
            {
                "name": attendee's name,
                "href": URL to the attendee,
            },
            ...
        ]
    }
    """
    return JsonResponse({"attendees": response})


def api_show_attendee(request, id):
    detail_attendee = Attendee.objects.get(id=id)
    return JsonResponse(
        {
            "name": detail_attendee.name,
            "email": detail_attendee.email,
            "company_name": detail_attendee.company_name,
            "created": detail_attendee.created,
            "conference": {
                "name": detail_attendee.conference.name,
                "href": detail_attendee.conference.get_api_url(),
            },
        }
    )
